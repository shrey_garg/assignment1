package exception;

/**
 * Custom exception thrown when user tries to get maximum and minimum from empty array
 * 
 * @author sgarg
 *
 */
public class EmptyArrayException extends Exception {}
