package exception;

/**
 * Custom exception thrown when user inputs an invalid input
 * 
 * @author sgarg
 *
 */
public class InvalidInputException extends Exception {}
