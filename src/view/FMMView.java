package view;

/**
 * FMMView plays the role of view
 * Receives command to display data from the controller
 * 
 * @author sgarg
 *
 */
public class FMMView {
	
	public void displayMessage(String message) {
		System.out.print(message);
	}
	
}
