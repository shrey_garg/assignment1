package model;

import exception.EmptyArrayException;
import exception.InvalidInputException;

/**
 * FMMModel class plays the role of the model class.
 * Receives data input coming in via controller and performs actions on it
 * Has a reference to the ExpandableArray model class
 * 
 * @author sgarg
 *
 */
public class FMMModel {

	ExpandableArray expandableArray = new ExpandableArray();

	public double validateInput(String input) throws InvalidInputException {
		try {
			return Double.parseDouble(input);
		} catch (Exception e) {
			/*
			 * Throws an exception when the input received is invalid
			 */
			throw new InvalidInputException();
		}
	}

	public void handleInput(String input) throws InvalidInputException {
		double value = validateInput(input);
		expandableArray.add(value);
	}

	public String getArrayString() {
		return expandableArray.toString();
	}

	public double[] findMaxMin() throws EmptyArrayException {
		return expandableArray.findMaxMin();
	}

}
