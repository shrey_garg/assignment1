package model;

/**
 * Constants file containing constant messages to be shown for the User Interface
 * 
 * @author sgarg
 *
 */
public class Constants {

	public final static String INSTRUCTIONS = "\nEnter integer or decimal values and press enter key to add values to the array."
			+ "\nEnter dollar character ('$') to stop entering and get Minimum and Maximum value of the array."; 
	public final static String INPUT = "\n\nEnter a number or dollar character ($) to get result : ";
	public final static String CORRECT_INPUT = "\nValue added to array";
	public final static String INCORRECT_INPUT = "\nIncorret input entered.";
	public final static String DISPLAY_ARRAY = "\nEntered array : %s";
	public final static String FINAL_ARRAY = "\n\n----------------------\n\nFinal array : %s";
	public final static String OUTPUT = FINAL_ARRAY + "\n\nMaximum value : %s\nMinimum value : %s\n\nThank you.";
	public final static String MINIMUM_ARRAY_SIZE = "\n\nYou need to add at least one value to show output.\nTry again.\n\n";
	public final static String END_CHAR = "$"; 
}
