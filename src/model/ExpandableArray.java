package model;

import java.util.Arrays;

import exception.EmptyArrayException;

/**
 * Model class for Expandable Array
 * 
 * @author sgarg
 *
 */
public class ExpandableArray {

	private final int INITIAL_SIZE = 10;
	private double values[] = new double[INITIAL_SIZE];
	private int index = 0;

	public ExpandableArray() {
	}

	/*
	 * Method to add element to the array, first checks if array has space left then adds element 
	 */
	public void add(double value) {
		ensureSize();
		values[index++] = value;
	}

	/*
	 * Method to find the minimum and maximum values from array
	 */
	public double[] findMaxMin() throws EmptyArrayException {
		if (index > 0) {
			double max = Double.MIN_VALUE;
			double min = Double.MAX_VALUE;
			for (int i = 0; i < index; i++) {
				if (max < values[i])
					max = values[i];
				if (min > values[i])
					min = values[i];
			}
			return new double[] { max, min };
		} else {
			/*
			 * Throw custom exception if array is empty
			 */
			throw new EmptyArrayException();
		}
	}

	private void ensureSize() {
		/*
		 * If array is full, new array is created with twice the size and old elements are copied to the new array
		 */
		if (index == values.length) {
			int newSize = values.length * 2;
			double newValues[] = new double[newSize];
			System.arraycopy(values, 0, newValues, 0, values.length);
			values = newValues;
		}
	}

	@Override
	public String toString() {
		/*
		 * New array created with only filled values and blank values avoided to print
		 */
		double newValues[] = new double[index];
		System.arraycopy(values, 0, newValues, 0, index);
		return Arrays.toString(newValues);
	}

}
