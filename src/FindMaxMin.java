import controller.FMMController;
import exception.InvalidInputException;
import model.FMMModel;
import view.FMMView;

public class FindMaxMin {
	
	/**
	 * Starting point of the application
	 * 
	 * @param args
	 * @throws InvalidInputException
	 */
	public static void main(String[] args) {
		FMMView view = new FMMView();
		FMMModel model = new FMMModel();
		FMMController controller = new FMMController(view, model);
		controller.start();
	}

}
