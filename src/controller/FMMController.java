package controller;

import java.util.Scanner;

import exception.EmptyArrayException;
import exception.InvalidInputException;
import model.FMMModel;
import static model.Constants.*;
import view.FMMView;

/**
 * FMMController class has references to the view and the model.
 * Controls the data flow into the model and updates the view to display data.
 * Helps keep the view and model separated.
 * 
 * @author sgarg
 *
 */
public class FMMController {

	private FMMView view;
	private FMMModel model;
	private static Scanner scanner = new Scanner(System.in);

	public FMMController(FMMView view, FMMModel model) {
		super();
		this.view = view;
		this.model = model;
	}

	public void start() {
		view.displayMessage(INSTRUCTIONS);
		view.displayMessage(INPUT);
		String value = scanner.next();
		/*
		 * Keeps running until user inputs the END_CHAR
		 */
		while (!END_CHAR.equals(value)) {
			try {
				model.handleInput(value);
				/*
				 * Updates the view to display message
				 */
				view.displayMessage(CORRECT_INPUT);
				String currentArrayToString = model.getArrayString();
				String currentArrayMesssage = String.format(DISPLAY_ARRAY, currentArrayToString);
				view.displayMessage(currentArrayMesssage);
			} catch (InvalidInputException e) {
				/*
				 * Handles incorrect inputs from user, such as - 'a', '@' etc 
				 */
				view.displayMessage(INCORRECT_INPUT);
			}
			view.displayMessage(INPUT);
			value = scanner.next();
		}
		try {
			double[] maxmin = model.findMaxMin();
			String outputMessage = String.format(OUTPUT, model.getArrayString(), maxmin[0], maxmin[1]);
			view.displayMessage(outputMessage);
		} catch (EmptyArrayException e) {
			/*
			 * Updates view to display warning that max and min cannot be found when the array size is 0
			 */
			view.displayMessage(MINIMUM_ARRAY_SIZE);
			start();
		}
	}
}
